﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace EhehDada {

	//enum for logging.
	public enum LogLevel {ALL = 0, WARNINGS = 1, ERRORS = 2, OFF = 3};

	//a class to store a high score table. with a convenience tostring method for printing.
	[System.Serializable]
	public class EDD_Table {
		public List<EDD_TableRow> values;

		public override string ToString(){
			if(values == null)
				return "null, no data.";

			string s = "TABLE ----\n";
			for(int i = 0; i < values.Count; i++) {
				EDD_TableRow row = values[i];
				s+= string.Format("{0}:{1} (in {2})\n", row.by_whom, row.score, row.locale);
			}
			s += "---------";
			return s;
		}  

	}

	//a row of a high score table.
	[System.Serializable]
	public class EDD_TableRow {
		public double score; //the score which was recorded by the player
		public string by_whom; //the name of the player
		public int when_happened; //the date time of this record in Unix format, from UTC time zone
		public string locale; //the locale of the player's device

		//optional
		public uint ip4; //an unsigned integer with the IPv4 from your player's system
		public ulong ip6; //an unsigned 128 bits integer with the IPv6 from your player's system
	
		//added locally
		public int place;
		public string placeString;
	}
	
	//a json response for the elegibility api call.
	[System.Serializable]
	public class EDD_Elegibility {
		public bool is_elegible; //could you be on the high score table?
	}

	//a json response for the timestamp api call. 
	[System.Serializable]
	public class EDD_Timestamp {
		public double timestamp; //in unix time (milliseconds).
	}

	//a json response for the add record api call. 
	[System.Serializable]
	public class EDD_PositionAdded {
		public int position; //indicates the position inside of the highscores table where the record has been inserted, or null if it was rejected. A first place score will return 0, second place will return 1, etc.

	}

	//a json container for the parameters of the addrecord api call.
	[System.Serializable]
	public class EDD_AddRecord_Body {
		public double score; // the score which was recorded by the player
		public string by_whom; //the name of the player
		public double when_happened; //the date time of this record in Unix format from UTC time zone
		public string locale; //the locale of the player's device
	}
	
	//a json container for the service startup.
	[System.Serializable]
	public class EDD_InitParams {
		public string _publicSharedSecret;
		public string _privateSharedSecret;
		public string _tableName;
		public string _tableKey;
	}

}