﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;


namespace EhehDada {

	/*
		This object handles the lifecycle of a rest request. 
		It uses a EDD_RestRequest_Params that tell it how to
		perform the request.

		It has access to the context it was created in (the 
		EDD_HighScoreService instance), and the params that determine how
		the call is performed. It is stateful, and is used once.
	*/
	[System.Serializable]
	public class EDD_RestRequest {
		public EDD_Request_Params _params;
		public string _response;
		public UnityWebRequest _www;
		public bool _aborted = false;

		public EDD_RestRequest(EDD_Request_Params p) {
			_params = p;
		}

		public T Parse<T>() {
			if(_aborted)
				return default(T);

			T parsed = JsonUtility.FromJson<T>(_response);
			if(parsed == null) {
				_params._context.Log(_params._name + " failed to parse json. Invalid json was: " + _response, LogLevel.ERRORS);
			} else {
				// _params._context.Log(_params._name + " succeeded.");
			}
			return parsed;
		}

		private bool IsComplete(UnityWebRequest r) {
			if(r == null)
				return true;
			else if(r.isDone || _aborted)
				return true;
			#if UNITY_2017_1_OR_NEWER
			else if(r.isHttpError || r.isNetworkError)
				return true;
			#endif
			else if(!string.IsNullOrEmpty(r.error))
				return true;
			else 
				return false;
		}

		public bool Stop() {
			if(IsComplete(_www)) {
				return false;
			} else { //we need to stop it.
				_www.Abort();
				_aborted = true;
				return true;
			}
		}

		public IEnumerator Perform() {
			_response = null; //on re-use.

			if(Application.internetReachability == NetworkReachability.NotReachable) { 
				_params._context.Log("Internet is disconnected. Cannot perform request.", LogLevel.ERRORS);
				yield break;
			}
			
			_params._context.AddOutgoing(this);

			if(_www != null) {
				Stop();
				while(!IsComplete(_www))
					yield return 0;
				_www = null;
			}

			_www = new UnityWebRequest(_params._urlBase + _params._uri);

			byte[] bodyRaw = null;

			if(_params._RESTVerb == UnityWebRequest.kHttpVerbPOST) {
				if(string.IsNullOrEmpty(_params._body)) {
					_params._context.Log("Posting empty body?", LogLevel.WARNINGS);
				} else {
					string json = _params._body;
					bodyRaw = new System.Text.UTF8Encoding().GetBytes(json);
				}
				_www.SetRequestHeader("Content-Type", "application/json");
			}

    		_www.uploadHandler   = (UploadHandler) new UploadHandlerRaw(bodyRaw);
    		_www.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
			_www.method          = _params._RESTVerb;

			//set request headers -- needs to be done in this order, because fingerprint calc updates timestamp.
			_www.SetRequestHeader("Fingerprint", _params.GetFingerprint());	// fingerprint: a value a calculated by you, as shown in the request authentication
			_www.SetRequestHeader("Timestamp", (_params._epoch).ToString()); // timestamp: a value that must be exactly the same value as the one used for calculating the fingerprint, and must be given from the time zone UTC
			_www.SetRequestHeader("Tablekey", _params._info._tableKey); // tablekey: the unique name for your table, given to you by us

			float startTime = Time.realtimeSinceStartup;

			#if UNITY_2017_1_OR_NEWER
            	yield return _www.SendWebRequest();
			#else
				yield return _www.Send();
			#endif
			float duration = Time.realtimeSinceStartup - startTime;
			duration = Mathf.Floor(duration * 100f) / 100f;
			
			if (_aborted) {
				//no change
            } else if (!string.IsNullOrEmpty(_www.error)) {
                _params._context.Log(_www.error, LogLevel.ERRORS);
            } else {
				_response = _www.downloadHandler.text;
				_params._context.Log("Success! Took " + duration + "s. Response = " + _response);
			}
		}
    }

	/* 
		An intermediate object used by the request object. 
		It contains all the data needed to create a rest request.
		Each one has a unique name (which it derives from the context).

		When you Stop or try again in the high score service, this is
		being passed back into the service, and put into new Requests.

	*/
	[System.Serializable]
	public class EDD_Request_Params {

		public EDD_HighScoreService _context;
		public EDD_InitParams _info;

		public string _body;
		public string _uri;
		public double _epoch;
		public string _name; //names have tags and numbers. Ex: GetTable_12234234_1.

		public readonly string _RESTVerb;
		public readonly string _urlBase = "https://highscores.ehehdada.com";



		public EDD_Request_Params(EDD_HighScoreService context, string name, string uri, string body = "", string verb = UnityWebRequest.kHttpVerbGET) {
			_context = context;
			_info = context._currParms;
			_uri = uri;
			_body = body;
			_name = name + "_" + _context.GetName();
			_RESTVerb = verb;
		}


		/*
		hashed_shared_secret <= SHA256( shared_secret )
		string_to_hash <= uri "|" requestBody "|" timestamp "|" table_key "|" hashed_shared_secret
		fingerprint <= SHA256( string_to_hash "|" string_length(string_to_hash) )
		*/
		public string GetFingerprint() {
			_epoch = (double)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
			_epoch *= 1000; //convert to ms
			
			string[] pieces = new string[] {
				_uri, _body, _epoch.ToString(), _info._tableKey, _info._publicSharedSecret
			};
			string hashed = string.Join("|", pieces);
			string fingerprint =  CryptoUtils.SHA256(hashed + "|" + hashed.Length.ToString());
			return fingerprint;
		}
	}
}
