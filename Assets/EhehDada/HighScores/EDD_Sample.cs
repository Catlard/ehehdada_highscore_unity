﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EhehDada;
using System;

namespace EhehDada {
	public class EDD_Sample : MonoBehaviour {
		
		[TextArea(7,10)] public string _key;
		public GameObject _rowPrefab;

		EDD_HighScoreService _service;

		// Use this for initialization
		public IEnumerator Start () {

			//initialize the service.
			_service = GameObject.Find("HighScoreService").GetComponent<EDD_HighScoreService>(); //get the service component.
			_service.Init(_key);

			yield return _service.StartCoroutine(_service.GetTable());
			RedrawUI();
		}

		public void AddNewRecord() {
			print("Trying to add new record.");
			StopCoroutine("AddRecordToSampleRoutine");
			StartCoroutine("AddRecordToSampleRoutine");
		}

		public void DoTest() {
			StopCoroutine("TestRoutine");
			StartCoroutine("TestRoutine");
		}

		private IEnumerator TestRoutine() {
			
			//perform all function calls in the service.
			yield return _service.StartCoroutine(_service.GetTable());
			yield return _service.StartCoroutine(_service.CheckElegibility(100, "Simon"));
			yield return _service.StartCoroutine(_service.GetServerTime());
			yield return StartCoroutine("AddRecordToSampleRoutine");

			// perform a bunch of requests, then stop them from completing.
			_service.StartCoroutine(_service.GetTable());		
			_service.StartCoroutine(_service.GetTable());		
			_service.StartCoroutine(_service.GetTable());
			List<EDD_Request_Params> unfinished = _service.Stop();	
			yield return _service.StartCoroutine(_service.TryAgain(unfinished));	

			RedrawUI(); // redraw!
		}

		private IEnumerator AddRecordToSampleRoutine() {
			//make an AddRecord object. this is used to make the body of the request.
			EDD_AddRecord_Body bodyObj = new EDD_AddRecord_Body();
			bodyObj.by_whom = StringUtils.RandomName();
			bodyObj.score = UnityEngine.Random.Range(0, 1000);
			bodyObj.when_happened = (double)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
			bodyObj.locale = "?";
			
			//send it to the server.
			yield return StartCoroutine(_service.AddRecordToTable(bodyObj));

			//print where you were to the console.
			string place = StringUtils.GetPlaceString(_service._positionAdded.position);
			print(bodyObj.by_whom + " was in: " + place + " place!");
		

			//retrieve the data.
			yield return _service.StartCoroutine(_service.GetTable());

			RedrawUI(); //redraw.
		}
			
			
		//rebuild the rows in the UI sample table...
		public void RedrawUI() {

			EDD_Table table = _service._latestTable;

			//clear the current rows
			RectTransform rowParent = GameObject.Find("RowContainer").GetComponent<RectTransform>();
			for(int i = rowParent.childCount-1; i >= 0; i--) {
				GameObject.Destroy(rowParent.GetChild(i).gameObject);
			}

			//counters for positioning rows.
			float yPos = 0;
			float yGap = 50;

			//populate with new rows.
			for(int i = 0; i < table.values.Count; i++) {
				//add a row object
				RectTransform rowRT = GameObject.Instantiate(_rowPrefab).GetComponent<RectTransform>();
				rowRT.SetParent(rowParent, false);
				rowRT.transform.localPosition = Vector3.zero;

				//give it data.
				UITableRow row = rowRT.GetComponent<UITableRow>();
				row.Init(table.values[i]);
				// 							//top  left
				rowRT.offsetMax = new Vector2(0, yPos);
				rowRT.offsetMin = new Vector2(0, rowRT.offsetMin.y);
				rowRT.sizeDelta = new Vector2(rowRT.sizeDelta.x, 50);

				yPos -= yGap;
				
				if(i == _service._positionAdded.position-1) {
					row.Highlight();
				}
			}

			//all done!
		}
	}
}
