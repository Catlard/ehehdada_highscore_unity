﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace EhehDada {
    public static class StringUtils {
        
        private static System.Random random = new System.Random((int)DateTime.Now.Ticks);//thanks to McAden
        private static char[] vowels = { 'a', 'e', 'i', 'o', 'u' };
        private static string[] vowelDigraphs = { "au", "ee", "ai", "oo", "ui" };
        private static char[] consonants = { 'b', 'c', 'd', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z' };
        private static string[] startPairs = { "th", "ch", "sh", "st", "sc", "sk", "sl", "pl", "tr", "pr", "cl", "fr" };
        private static string[] endPairs = { "th", "ch", "sh", "rn", "rg", "rd", "rk", "rt", "rp", "lp", "lm", "lk" };
        
        private static string GetVowelSound() {
            string res = "";
            int i = random.Next(vowels.Length * 3 + vowelDigraphs.Length);
            if (i >= vowels.Length) i -= vowels.Length;
            if (i >= vowels.Length) i -= vowels.Length;
            if (i >= vowels.Length) {
                res += vowelDigraphs[i - vowels.Length];
            } else {
                res += vowels[i];
            }
            return res;
        }
        
        private enum ConsonantSoundType { Start, Middle, End }

        private static string GetConsonantSound(ConsonantSoundType type) {
            string res = "";
            if (type == ConsonantSoundType.Start) {
                int n = consonants.Length + startPairs.Length;
                int i = random.Next(n);
                if (i < consonants.Length) {
                    res += consonants[i];
                } else {
                    res += startPairs[i - consonants.Length];
                }
            } else if (type == ConsonantSoundType.End) {
                int n = consonants.Length + endPairs.Length;
                int i = random.Next(n);
                if (i < consonants.Length) {
                    res += consonants[i];
                } else {
                    res += endPairs[i - consonants.Length];
                }
            } else {
                int i = random.Next(consonants.Length);
                res += consonants[i];
            }
            return res;
        }
        
        public static string RandomName() {
            string res = "";
            if (random.Next(6) != 0) {
                res += GetConsonantSound(ConsonantSoundType.Start);
            }
            res += GetVowelSound();
            res += GetConsonantSound(ConsonantSoundType.Middle);
            res += GetVowelSound();
            if (random.Next(4) != 0) {
                res += GetConsonantSound(ConsonantSoundType.End);
            }
            char[] ca = res.ToCharArray();
            ca[0] = char.ToUpper(res[0]);
            return new string(ca);
        }


        public static string GetPlaceString(int number) {
            int baseTen = number % 10;
            string suffix = "th";
            if(number < 14 && number > 10)
                suffix = "th";
            else if(baseTen == 1)
                suffix = "st";
            else if(baseTen == 2)
                suffix = "nd";
            else if(baseTen == 3)
                suffix = "rd";

            return number.ToString() + suffix;
        }
    }
}