﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace EhehDada {

    public class UITableRow : MonoBehaviour
    {
        private void SetText(string areaName, string s) {
            RectTransform areaRT = (RectTransform) transform.Find(areaName);
            RectTransform textRT = (RectTransform) areaRT.Find("Text");
            Text text = textRT.GetComponent<Text>();
            text.text = s;
        }

        public void Init(EDD_TableRow rowData) {
            Image bg = GetComponent<Image>();
            bg.color = new Color(Random.value, Random.value, Random.value, .5f);
            SetText("PlaceArea", rowData.placeString);
            SetText("NameArea", rowData.by_whom);
            SetText("ScoreArea", rowData.score.ToString());
        }

        public void Highlight() {
            transform.Find("Highlight").GetComponent<Image>().enabled = true;
        }

        
    }

}
