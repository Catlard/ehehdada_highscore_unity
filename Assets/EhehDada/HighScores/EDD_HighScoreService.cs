﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace EhehDada {

	public class EDD_HighScoreService : MonoBehaviour {

		//this can filter the messages you will receive from this service. With "ALL", you see messages about all the activities of the plugin.
		public LogLevel _logLevelShown = LogLevel.ALL;
		[HideInInspector] public EDD_InitParams _currParms; //where your table's credentials are stored.

		//these are where the most recent data pulled from the server 
		//is stored. if one is null, that means it was requested yet. 
		//They are updated when their associated request function is
		//called. You use these to access downloaded information.
		public EDD_Table _latestTable;
		public EDD_Elegibility _wasElegible;
		public EDD_Timestamp _serverTime;
		public EDD_PositionAdded _positionAdded;

		//where pending requests are stored. you shouldn't have to go in here.
		private List<EDD_RestRequest> _outgoing;

		//these are ID tags used to identify this instance of the service.
		private long _tagCounter = 0;
		private int _serviceID;
		
		//you can init with json this way.
		public void Init(string jsonKey) {
			EDD_InitParams parms = JsonUtility.FromJson<EDD_InitParams>(jsonKey);
			if(parms == null) {
				Log("Your json key during init was malformed. The high score service was not initialized. Make sure it is correct, or make a new table, copy it, and try again.", LogLevel.ERRORS);
				return;
			} else {
				Init(parms);
			}
		}

		//or you can init with an object with filled in fields.
		public void Init(EDD_InitParams parms) {
			_currParms = parms;
			_outgoing = new List<EDD_RestRequest>();
			_serviceID = Random.Range(0, 1000000);
			Log("Initted for table " + parms._tableName);
		}

		//produces unique tags for request objects.
		public string GetName() {
			_tagCounter++;
			return _serviceID + "_" + _tagCounter;
		}

		//stops activities of the service, and returns a list of params.
		//these params can be serialized and used again when the app has
		//internet, or when it resumes.
		public List<EDD_Request_Params> Stop() {
			int count = 0;
			List<EDD_Request_Params> unfinished = new List<EDD_Request_Params>();
			for(int i = 0; i < _outgoing.Count; i++) {
				EDD_Request_Params stopMe = _outgoing[i]._params;
				bool wasStopped = _outgoing[i].Stop();
				if(wasStopped) {
					unfinished.Add(stopMe);
					count++;
				}
			}
			Log("Stopped " + count + " pending network requests.");
			_outgoing = new List<EDD_RestRequest>();
			return unfinished;
		}

		//collects outgoing requests.
		public void AddOutgoing(EDD_RestRequest req) {
			_outgoing.Add(req);
		}

		//works together with the Stop function. You feed request params
		//back into this, and it performs them again.
		public IEnumerator TryAgain(List<EDD_Request_Params> unfinished) {
			for(int i = 0; i < unfinished.Count; i++) {
				EDD_Request_Params parms = unfinished[i];
				Log("Trying request again: " + parms._name);
				parms._context = this;
				EDD_RestRequest req = new EDD_RestRequest(parms);
				StartCoroutine(req.Perform());
			}
			yield return 0;
		}

		//Get a high score table object from the servers, with the latest data.
		//The result of this is placed in the _latestTable variable.
		public IEnumerator GetTable() {
			string uri = string.Format(
				"/api/{0}?table_key={1}", 
				_currParms._tableName, _currParms._tableKey
			);
			EDD_Request_Params parms = new EDD_Request_Params(this, "GetTable", uri);
			EDD_RestRequest request = new EDD_RestRequest(parms);
			yield return StartCoroutine(request.Perform());
			if(request._aborted) yield break;
			request._response = JSONUtils.WrapArray(request._response);
			_latestTable = request.Parse<EDD_Table>();
			for(int i = 0; i < _latestTable.values.Count; i++) {
				_latestTable.values[i].placeString = StringUtils.GetPlaceString(i+1);
				_latestTable.values[i].place = i + 1;
			}
		}

		//Check to see if your score would fit in the table.
		//The result is placed in the _wasElegible variable.
		public IEnumerator CheckElegibility(double score, string by_whom) {
			string uri = string.Format(
				"/api/{0}/check?score={1}&by_whom={2}&table_key={3}",
				_currParms._tableName, score, by_whom, _currParms._tableKey
			);
			EDD_Request_Params parms = new EDD_Request_Params(this, "CheckElegibility", uri);
			EDD_RestRequest request = new EDD_RestRequest(parms);
			yield return StartCoroutine(request.Perform());
			if(request._aborted) yield break;
			_wasElegible = request.Parse<EDD_Elegibility>();
		
		}

		//a convenience method that extracts information from the object used by
		//the addrecord function.
		public IEnumerator CheckElegibility(EDD_AddRecord_Body b) {
			yield return StartCoroutine(CheckElegibility(b.score, b.by_whom));
		}

		//Check the latest server time.
		//The result is placed in the _serverTime variable.
		public IEnumerator GetServerTime() {
			string uri = "/timestamp";
			EDD_Request_Params parms = new EDD_Request_Params(this, "GetServerTime", uri);
			EDD_RestRequest request = new EDD_RestRequest(parms);
			yield return StartCoroutine(request.Perform());
			if(request._aborted) yield break;
			_serverTime = request.Parse<EDD_Timestamp>();

		}

		//Add a score (a record) to the table.
		//The result, which is your place in the table, is returned.
		//It is stored in the _positionAdded variable. 
		public IEnumerator AddRecordToTable(EDD_AddRecord_Body bodyObject) {
			string uri = "/api/" + _currParms._tableName;
			string bodyJSON = JsonUtility.ToJson(bodyObject);
			EDD_Request_Params parms = new EDD_Request_Params(this, "AddRecordToTable", uri, bodyJSON, UnityWebRequest.kHttpVerbPOST);
			EDD_RestRequest request = new EDD_RestRequest(parms);
			yield return StartCoroutine(request.Perform());
			if(request._aborted) yield break;
			_positionAdded = request.Parse<EDD_PositionAdded>();
			_positionAdded.position++; //comes in zero-indexed.
		}

		//This service's log function.
		public void Log(string msg, LogLevel level = LogLevel.ALL) {
			float timeSinceStart = Mathf.Floor(Time.realtimeSinceStartup * 100f)/100f;
			string prefix = "<color=gray>MSG";
			if(level == LogLevel.WARNINGS)
				prefix = "<color=yellow>WARN";
			else if(level == LogLevel.ERRORS)
				prefix = "<color=red>ERR";

			prefix = "HS " + prefix + "(@" + timeSinceStart + "s): </color>";

			if(level >= _logLevelShown)
				Debug.Log(prefix + msg);
		}
	}

}
