
////////////////////////////////////
ABOUT THIS PLUGIN
////////////////////////////////////

This library allows you to work with the Lucentinian Works Co Ltd high scores service. It works in Unity 5.3 personal edition and up. At the moment it uses the UnityWebRequest object. We will add a WWW compliant version if one is requested.

The documentation, api, and online demos can be found at:
https://highscores.ehehdada.com/

You can create new tables for free at:
https://highscores.ehehdada.com/new_account

Here is a tutorial video showing how it is integrated into Unity:
https://www.catlard.com/code

Here is the bitbucket repo where this code comes from:
https://bitbucket.org/Catlard/ehehdada_highscore_unity/src/master/

Although this high score service and library is provided free of charge, we do a little cryptomining whenever you create a new high score table (usually, you only need one per game) or when you need to reset it. But it's free to use otherwise! Also, wee reserve the right to contact you about your high score tables and ask you to set up a payment plan. We will do this if your usage exceeds <max calls per period>. 


////////////////////////////////////
ABOUT THE SAMPLE SCENE.
////////////////////////////////////

Look at the Sample.cs file, and the Sample Unity scene, for advice about how to get started interacting
with HighScoreService.cs.




////////////////////////////////////
CHANGELOG!
////////////////////////////////////


2.00 Refactor, works with UnityWebRequest.
———————————————————————————
--All classes refactored, working properly.
--Stop function works to halt all pending requests.
--Requests can be retried later with TryAgain.
--Can access any pending calls.

1.23 Bugfix -- Get table
———————————————————————————
—-Made sure the view was getting the table correctly -- no more null entries at the end.
--Moved some things out of HighScoreUtils
--Gave some non-generic names to classes.
--The functionality of the sample is now seperate from the library for high score utils.
TODO Fix table Delete functionality.
TODO Provide sample graphical hs using new Unity UI. 

1.21: MVC Implemented in Sample Scene
———————————————————————————
—-Added a model, control, and view for easier dissection of the operations performed by the model.
—-Added a prefab that users can drop in their scene to perform operations, called “HighScoreUtils”.


